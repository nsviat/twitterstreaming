package com.kafka.twitter;

import com.google.gson.JsonParser;

public class TweetsDataExtractor {

    static String extractIdFromTweet(String tweetJson){
        JsonParser jsonParser = new JsonParser();

        return jsonParser.parse(tweetJson)
                .getAsJsonObject()
                .get("id_str")
                .getAsString();
    }

}
