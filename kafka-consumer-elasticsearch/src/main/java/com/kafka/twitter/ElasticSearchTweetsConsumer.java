package com.kafka.twitter;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.time.Duration;

public class ElasticSearchTweetsConsumer {

    private static final String ELASTICSEARCH_INDEX_NAME = "ordinary_tweets";
    private static final String TOPIC_NAME = "ordinaryTweets";

    public static void main(String[] args) throws IOException {

        Logger logger = LoggerFactory.getLogger(ElasticSearchTweetsConsumer.class.getName());
        RestHighLevelClient client = TwitterRestClient.createClient();

        KafkaConsumer<String, String> consumer = Consumer.createConsumer(TOPIC_NAME);

        ElasticsearchProducer.runProducer(consumer, logger, client, ELASTICSEARCH_INDEX_NAME);
    }
}