package com.kafka.twitter;

import com.google.gson.JsonParser;

import javax.xml.stream.FactoryConfigurationError;

public class FilterKafkaHelper {

    private final long minFollowers;
    private final long maxFollowers;

    public FilterKafkaHelper(long minFollowers, long maxFollowers) {
        this.minFollowers = minFollowers;
        this.maxFollowers = maxFollowers;
    }

    boolean filterByUserFollowers(String key, String tweet){

        JsonParser jsonParser = new JsonParser();
        try {
                int followers_count = jsonParser.parse(tweet)
                        .getAsJsonObject()
                        .get("user")
                        .getAsJsonObject()
                        .get("followers_count")
                        .getAsInt();

                return this.minFollowers <= followers_count && followers_count >= this.maxFollowers;
            }
            catch (NullPointerException e){
                return false;
            }

    }
}
