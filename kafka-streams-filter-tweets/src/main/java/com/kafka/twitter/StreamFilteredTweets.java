package com.kafka.twitter;

import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.KStream;

import java.util.Properties;

public class StreamFilteredTweets {

    public static void main(String[] args) {
        Properties properties = BaseKafkaProperties.createDefaultProperties();

        StreamsBuilder streamsBuilder = new StreamsBuilder();

        FilterKafkaHelper popularTweets = new FilterKafkaHelper(1, 9999);

        KStream<String, String> inputTopic = streamsBuilder.stream("tweets");
        KStream<String, String> filteredStream = inputTopic.filter(
                popularTweets::filterByUserFollowers
        );
        filteredStream.to("ordinaryTweets");

        KafkaStreams kafkaStreams = new KafkaStreams(
                streamsBuilder.build(),
                properties
        );

        kafkaStreams.start();
    }

}
