package com.kafka.twitter;

import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.KStream;

import java.util.Properties;

public class StreamImportantFilteredTweets {

    public static void main(String[] args) {

        Properties properties = BaseKafkaProperties.createDefaultProperties();

        StreamsBuilder streamsBuilder = new StreamsBuilder();

        FilterKafkaHelper popularTweets = new FilterKafkaHelper(10000, 999999999);

        KStream<String, String> inputTopic = streamsBuilder.stream("tweets");
        KStream<String, String> filteredStream = inputTopic.filter(
                // filter for tweets which has a user of over 10000 followers
                popularTweets::filterByUserFollowers
        );
        filteredStream.to("importantTweets");

        KafkaStreams kafkaStreams = new KafkaStreams(
                streamsBuilder.build(),
                properties
        );

        kafkaStreams.start();
    }

}
